﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Models;

namespace TaskApi.Context
{
    public class TaskApiDbContext : DbContext
    {
        public TaskApiDbContext(DbContextOptions<TaskApiDbContext> options) : base(options)
        {

        }
        public DbSet<Task1> Tasks { get; set; }
        public DbSet<SubTask> SubTasks { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
