﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubTaskController : ControllerBase
    {
        // Add required dependencies here
        ISubTask _repo;
        public SubTaskController(ISubTask repo)
        {
            _repo = repo;
        }

        [HttpPost]

        public IActionResult AddTask(SubTask sbtask)
        {
            _repo.AddSubTask(sbtask);
            return Created("Created", sbtask);
        }

        [HttpGet("{id}")]
        public IActionResult GetSubTasks(int id)
        {
            if (_repo.GetSubTasksByTask(id).ToList().Count() != 0)
            {
                return Ok(_repo.GetSubTasksByTask(id).ToList());
            }
            else { return NotFound("No records found."); }
        }
    }
}
